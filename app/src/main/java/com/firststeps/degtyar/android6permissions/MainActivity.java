package com.firststeps.degtyar.android6permissions;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_READ_CONTACTS = 10;
    private static final int REQUEST_SEND_SMS = 20;

    static final int PICK_CONTACT = 1;

    Button contactsButton;
    Button sendButton;
    TextView smsBody;
    TextView receiver;

    MainActivity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        contactsButton = (Button) findViewById(R.id.buttonContacts);
        sendButton = (Button) findViewById(R.id.smsSendButton);
        smsBody = (TextView) findViewById(R.id.smsBody);
        receiver = (TextView) findViewById(R.id.editTextPhone);


        contactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (permissionRequest(activity, Manifest.permission.READ_CONTACTS, REQUEST_READ_CONTACTS)) {
                        Log.d("tag", "permission contacts granted");
                        getContacts();

                    } else {
                        Log.d("tag", "no contacts permission");
                    }

                } else {

                    getContacts();
                }
            }
        });


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (permissionRequest(activity, Manifest.permission.SEND_SMS, REQUEST_SEND_SMS)) {
                        Log.d("tag", "permission send sms granted");
                        sendMessage();

                    } else {
                        Log.d("tag", "no send_sms permission");
                    }

                } else {
                    sendMessage();
                }
            }

        });

    }


    private boolean permissionRequest(Activity activity, String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(activity,
                permission) != PackageManager.PERMISSION_GRANTED) {
            Log.d("tag", "! PackageManager.PERMISSION_GRANTED");

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) { //нужно ли показывать объяснение в необходимости запроса прав
                Log.d("tag", "Show an explanation");

            } else {

                Log.d("tag", "No explanation needed, we can request the permission.");
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }

        } else {

            Log.d("tag", "PERMISSION_GRANTED");
            return true;
        }

        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case REQUEST_READ_CONTACTS: {

                Log.d("tag", "REQUEST_READ_CONTACTS");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("tag", "PERMISSION_GRANTED");
                    getContacts();

                } else {

                    Log.d("tag", "!!! - PERMISSION_CONTACTS_NO_GRANTED");
                }

                break;
            }

            case REQUEST_SEND_SMS: {
                Log.d("tag", "REQUEST_SEND_SMS");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("tag", "PERMISSION_SEND_SMS_GRANTED");
                    sendMessage();

                } else {

                    Log.d("tag", "!!! - PERMISSION_SEND_SMS_NO_GRANTED");
                }
                break;
            }

        }
    }


    private void getContacts() {

        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error in intent : ", e.toString());
        }
    }

    private void sendMessage() {
        String phoneNo = receiver.getText().toString();
        String msg = smsBody.getText().toString();

        try {
            if (phoneNo.equals("") || msg.equals("")) {
                Toast.makeText(getApplicationContext(), "Phone number or message body is empty", Toast.LENGTH_LONG).show();

            } else {

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, null, null);
                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    //code
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    String cNumber = null;

                    Uri contactData = data.getData();
                    ContentResolver cr = getContentResolver();
                    Cursor c = cr.query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {

                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            cNumber = phones.getString(phones.getColumnIndex("data1"));

                            if (cNumber != null) {

                                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                receiver.setText(cNumber);
                                // Toast.makeText(this, "Name: " + name + ", Phone No: " + cNumber, Toast.LENGTH_SHORT).show();

                            }

                        }
                    }
                }
                break;
        }
    }


}
